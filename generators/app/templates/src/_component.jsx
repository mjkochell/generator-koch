import React, {Component} from 'react';
import {withStyles} from 'material-ui/styles'
import PropTypes from 'prop-types'

import styles from '<%= stylesFile %>'

@withStyles(styles)
export class <%= name %> extends Component {

  static propTypes = {
    classes: PropTypes.object.isRequired
  }

  render() {
    const {classes} = this.props

    return (
      <div className={classes.<%= className %>}>
        I am <%= name %>
      </div>
    );
  }
}

export default <%= name %>
