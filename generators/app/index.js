const Generator = require('yeoman-generator')
var _ = require('lodash')
var chalk = require('chalk')
var yosay = require('yosay')
var path = require('path')

module.exports = class extends Generator {
  prompting() {
    this.log(
      chalk.blue(' __  __')                                        + '\n' +
      chalk.blue('| | / /'   + ' ___ '   + ' ___' +  '| |' )       + '\n' +
      chalk.blue('| |/ / '   + '/ _ \\'  + '/ __' +  '| |__')      + '\n' +
      chalk.blue('|  _ \\  ' +  '(_) '   + ' (__' +  '| |\\ \\')   + '\n' +
      chalk.blue('|_|_\\_\\' + '\\___/'  + '\\___'+  '|_| \\ \\ ') + '\n' +
      chalk.blue(' / ___|___  _ __ ___  _ __   ___  _ __   ___ _ __ | |_') + '\n' +
      chalk.blue('| |   / _ \\| \'_ ` _ \\| \'_ \\ / _ \\| \'_ \\ / _ \\ \'_ \\| __|') + '\n' +
      chalk.blue('| |__| (_) | | | | | | |_) | (_) | | | |  __/ | | | |_') + '\n' +
      chalk.blue(' \\____\\___/|_| |_| |_| .__/ \\___/|_| |_|\\___|_| |_|\\__|') + '\n' +
      chalk.blue(' / ___| ___ _ __   __|_| __ __ _| |_ ___  _ __\'') + '\n' +
      chalk.blue('| |  _ / _ \\ \'_ \\ / _ \\ \'__/ _` | __/ _ \\| \'__|') + '\n' +
      chalk.blue('| |_| |  __/ | | |  __/ | | (_| | || (_) | |') + '\n' +
      chalk.blue(' \\____|\\___|_| |_|\\___|_|  \\__,_|\\__\\___/|_|') + '\n' +
    '');

    const prompts = [
      {
        type: 'input',
        name: 'name',
        message: 'First, what is the name of your Component?',
        default: 'Component'
      },
      {
        type: 'input',
        name: 'needsFolder',
        message: 'Create Folder?',
        default: 'Y'
      },
    ];

      return this.prompt(prompts)
        .then(props => {
        this.props = props;
    });
  }

  writing() {
    const needsFolder = this.props.needsFolder === 'Y'
    const name = this.props.name.substring(0, 1).toUpperCase() + this.props.name.substring(1)

    const cwd = process.cwd().split(path.sep).pop()

    const stylesFile = needsFolder? `./styles/${name}.styles`: `./${cwd}/styles.styles`
    const folder = needsFolder? `${name}/` : ''
    const className = name.substring(0, 1).toLowerCase() + name.substring(1) + 'Container'

    this.fs.copyTpl(
      this.templatePath('src/_component.jsx'),
      this.destinationPath(`${folder}${name}.jsx`), {
        name,
        stylesFile,
        className,
      }
    );

    this.fs.copyTpl(
      this.templatePath('src/_styles.js'),
      this.destinationPath(`${folder}styles/${name}.styles.js`), {
        className,
      }
    );

    this.fs.copyTpl(
      this.templatePath('src/_test.js'),
      this.destinationPath(`${folder}__tests__/${name}.test.js`), {
        name
      }
    )

  }
};
