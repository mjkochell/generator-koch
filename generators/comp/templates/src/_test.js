import React from 'react'
import { shallow } from 'enzyme'

import {<%= name %>} from '../<%= name %>'

describe('<<%= name %> />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<<%= name %> />)
  })

  it('renders', () => {
    expect(wrapper).toBeDefined()
  })
})
