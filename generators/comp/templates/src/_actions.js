function <%= name%>Action() {
  return {
    type: 'ACTION_NAME',
  }
}

export function <%= name %>ActionCreator() {
  return (dispatch, getState) => {
    return <%= name %>Action()
  }
}
