import React, {Component} from 'react'
import {withStyles} from 'material-ui/styles'
import PropTypes from 'prop-types'
<% if (needsConnect) { %>
import {connect} from 'react-redux'

import {<%= name %>ActionCreator} from './actions'
<%}%>
import styles from '<%= stylesFile %>'

@withStyles(styles)
export class <%= name %> extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    // <%= name %>ActionCreator: PropTypes.func.isRequired,
  }

  render() {
    const {classes} = this.props

    return (
      <div className={classes.<%= className %>}>
        I am <%= name %>
      </div>
    )
  }
}
<% if (needsConnect){%>
const mapStateToProps = (state) => {
  return {
    <%= name %>Reducer: state.<%= name %>Reducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    <%= name %>ActionCreator: () => dispatch(<%= name %>ActionCreator()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(<%= name %>)
<%}
else {%>
export default <%= name %>
<%}%>
