export function <%= name %>Reducer(state = {}, action) {
  switch (action.type) {
    case 'ACTION_NAME':
      return action.value
    default:
      return state
  }
}
