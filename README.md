# generator-koch

## Installation

First, install [Yeoman](http://yeoman.io) and generator-koch using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-koch
```

Then generate your new project:

```bash
yo koch
```

## License

MIT © [Michael Kochell]()

## TODO

 * introduce hierarchical prompts, using async/await
 * does this component have child components?
 * reducers and action creators with tests
